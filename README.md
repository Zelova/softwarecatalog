# Nica Software Catalog

This is the Nica Software Catalog, a centralised software installation and management program.

## Features

Nica SC will remotely install programs as defined by an administrator, and can install applications that the end-user chooses.

Nica SC can be a central place to install applications from a variety of sources including but may not be limited to:
* Microsoft Store
* `winget`
* Local (defined more at [Local Repository](#local-repository))
* Canonical App Centre (Snap Store (more defined at [Snap Store](#snap-store)))
* `apt`, `dnf`/`yum`, the AUR, `pacmac`, PPAs

As the list shows, Nica SC supports Windows and Linux.

